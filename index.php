<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');

    $object = new animal ("Shaun");

    echo "Name : $object->name <br>";
    echo "Legs : $object->legs <br>";
    echo "Cold Blooded : $object->cold_blooded <br><br>";

    $object2 = new frog ("Buduk");

    echo "Name : $object2->name <br>";
    echo "Legs : $object2->legs <br>";
    echo "Cold Blooded : $object2->cold_blooded <br>";
    echo $object2->jump() . "<br><br>"; 

    $object3 = new ape ("Kera Sakti");

    echo "Name : $object3->name <br>";
    echo "Legs : $object3->legs <br>";
    echo "Cold Blooded : $object3->cold_blooded <br>";
    $object3->yell();

?>